FROM node:13-alpine

ENV MONGO_DB_USERNAME=admin \
    MONGO_DB_PWD=passwords 

RUN mkdir -p /home/node-app

COPY ./app /home/app

CMD ["node", "home/app/server.js"]

